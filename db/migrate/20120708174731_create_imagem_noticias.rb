class CreateImagemNoticias < ActiveRecord::Migration
  def change
    create_table :imagem_noticias do |t|
      t.references :noticia
      t.has_attached_file :arquivo

      t.timestamps
    end
    add_index :imagem_noticias, :noticia_id
  end
end
