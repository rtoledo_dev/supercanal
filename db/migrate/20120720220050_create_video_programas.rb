class CreateVideoProgramas < ActiveRecord::Migration
  def change
    create_table :video_programas do |t|
      t.references :programa
      t.string :titulo
      t.has_attached_file :capa
      t.string :url
      t.boolean :pagina_inicial, default: false

      t.timestamps
    end
    add_index :video_programas, :programa_id
  end
end
