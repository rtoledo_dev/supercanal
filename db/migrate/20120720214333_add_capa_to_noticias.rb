class AddCapaToNoticias < ActiveRecord::Migration
  def up
    change_table :noticias do |t|
      t.string   "capa_file_name"
      t.string   "capa_content_type"
      t.integer  "capa_file_size"
      t.datetime "capa_updated_at"
    end
  end
  
  def down
    remove_column :noticias, :capa_file_name
    remove_column :noticias, :capa_content_type
    remove_column :noticias, :capa_file_size
    remove_column :noticias, :capa_updated_at
  end
end
