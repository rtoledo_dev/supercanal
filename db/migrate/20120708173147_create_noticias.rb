class CreateNoticias < ActiveRecord::Migration
  def change
    create_table :noticias do |t|
      t.references :programa
      t.string :titulo
      t.string :bigode
      t.text :conteudo
      t.datetime :publicada_em
      t.string :tag

      t.timestamps
    end
    add_index :noticias, :programa_id
  end
end
