class CamposEmVideoNoticias < ActiveRecord::Migration
  def up
    add_column :video_noticias, :pagina_inicial, :boolean, default: false
    add_column :video_noticias, :titulo, :string
  end

  def down
    remove_column :video_noticias, :pagina_inicial
    remove_column :video_noticias, :titulo
  end
end