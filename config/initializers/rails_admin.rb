# encoding: utf-8
# RailsAdmin config file. Generated on July 13, 2012 02:20
# See github.com/sferik/rails_admin for more informations

RailsAdmin.config do |config|

  # If your default_local is different from :en, uncomment the following 2 lines and set your default locale here:
  # require 'i18n'
  # I18n.default_locale = :de

  config.current_user_method { current_user } # auto-generated

  # If you want to track changes on your models:
  # config.audit_with :history, User

  # Or with a PaperTrail: (you need to install it first)
  # config.audit_with :paper_trail, User

  # Set the admin name here (optional second array element will appear in a beautiful RailsAdmin red ©)
  config.main_app_name = ['Supercanal', 'Admin']
  # or for a dynamic name:
  # config.main_app_name = Proc.new { |controller| [Rails.application.engine_name.titleize, controller.params['action'].titleize] }


  #  ==> Global show view settings
  # Display empty fields in show views
  # config.compact_show_view = false

  #  ==> Global list view settings
  # Number of default rows per-page:
  # config.default_items_per_page = 20

  #  ==> Included models
  # Add all excluded models here:
  config.excluded_models = [CategoriaNoticia]

  # Add models here if you want to go 'whitelist mode':
  # config.included_models = [Banner, Categoria, CategoriaNoticia, ImagemNoticia, Noticia, Programa, Usuario, VideoNoticia]

  # Application wide tried label methods for models' instances
  config.label_methods << :nome
  config.label_methods << :titulo

  #  ==> Global models configuration
  # config.models do
  #   # Configuration here will affect all included models in all scopes, handle with care!
  #

    # field :created_at do
    #   label "Criado"
    # end

    # list do
  #     # Configuration here will affect all included models in list sections (same for show, export, edit, update, create)
  #
  #     fields_of_type :date do
  #       # Configuration here will affect all date fields, in the list section, for all included models. See README for a comprehensive type list.
  #     end
  #   end
  # end
  #
  #  ==> Model specific configuration
  # Keep in mind that *all* configuration blocks are optional.
  # RailsAdmin will try his best to provide the best defaults for each section, for each field.
  # Try to override as few things as possible, in the most generic way. Try to avoid setting labels for models and attributes, use ActiveRecord I18n API instead.
  # Less code is better code!
  # config.model MyModel do
  #   # Cross-section field configuration
  #   object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
  #   label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
  #   label_plural 'My models'      # Same, plural
  #   weight -1                     # Navigation priority. Bigger is higher.
  #   parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
  #   navigation_label              # Sets dropdown entry's name in navigation. Only for parents!
  #   # Section specific configuration:
  #   list do
  #     filters [:id, :name]  # Array of field names which filters should be shown by default in the table header
  #     items_per_page 100    # Override default_items_per_page
  #     sort_by :id           # Sort column (default is primary key)
  #     sort_reverse true     # Sort direction (default is true for primary key, last created first)
  #     # Here goes the fields configuration for the list view
  #   end
  # end

  # Your model's configuration, to help you get started:

  # All fields marked as 'hidden' won't be shown anywhere in the rails_admin unless you mark them as visible. (visible(true))

  config.model User do
    label 'Administrador'
    label_plural 'Administradores'

    configure :reset_password_sent_at, :datetime do
      label 'Senha resetada em'
    end
  end

  config.model Banner do
    # Found associations:
    # Found columns:
      configure :id, :integer 
      configure :titulo, :string
      # configure :arquivo_file_name, :string         # Hidden 
      # configure :arquivo_content_type, :string         # Hidden 
      # configure :arquivo_file_size, :integer         # Hidden 
      # configure :arquivo_updated_at, :datetime         # Hidden 
      configure :arquivo, :paperclip 
      configure :ativo, :boolean 
      configure :created_at, :datetime do
        label 'Criado em'
      end
      configure :updated_at, :datetime do; label 'Atualizado em'; end   #   # Sections:
    list do; end
    export do; end
    show do; end
    edit do; end
    create do; end
    update do; end
  end
  config.model Categoria do
    # Found associations:
    # Found columns:
      configure :id, :integer 
      configure :nome, :string 
      configure :created_at, :datetime do
        label 'Criado em'
      end 
      configure :updated_at, :datetime do; label 'Atualizado em'; end   #   # Sections:
    list do; end
    export do; end
    show do; end
    edit do; end
    create do; end
    update do; end
  end
  config.model CategoriaNoticia do
    label 'Categoria de notícia'
    label_plural 'Categorias de notícia'
    # Found associations:
      configure :noticia, :belongs_to_association 
      configure :categoria, :belongs_to_association   #   # Found columns:
      configure :id, :integer 
      configure :noticia_id, :integer         # Hidden 
      configure :categoria_id, :integer         # Hidden 
      configure :created_at, :datetime do
        label 'Criado em'
      end 
      configure :updated_at, :datetime do; label 'Atualizado em'; end   #   # Sections:
    list do; end
    export do; end
    show do; end
    edit do; end
    create do; end
    update do; end
  end
  config.model ImagemNoticia do
    label 'Imagem de notícia'
    label_plural 'Imagens de notícia'
    # Found associations:
      configure :noticia#, :belongs_to_association   #   # Found columns:
      configure :id, :integer 
      #configure :noticia_id, :integer         # Hidden 
      # configure :arquivo_file_name, :string 
      # configure :arquivo_content_type, :string 
      # configure :arquivo_file_size, :integer 
      configure :arquivo, :paperclip
      configure :created_at, :datetime do
        label 'Criado em'
      end 
      configure :updated_at, :datetime do; label 'Atualizado em'; end   #   # Sections:
    list do; end
    export do; end
    show do; end
    edit do; end
    create do; end
    update do; end
  end
  config.model Noticia do
    label 'Notícia'
    label_plural 'Notícias'
    # Found associations:
      configure :programa#, :belongs_to_association 
      configure :video_noticias #, :has_many_association   #   # Found columns:
      configure :imagem_noticias #, :has_many_association   #   # Found columns:
      configure :id, :integer 
      configure :categoria_noticias do
        visible(false)
      end
      configure :categorias
      # configure :programa_id, :integer         # Hidden 
      configure :titulo, :string 
      configure :bigode, :string 
      configure :capa, :paperclip 
      configure :conteudo, :text 
      configure :publicada_em, :datetime 
      configure :tag, :string 
      configure :created_at, :datetime do
        label 'Criado em'
      end 
      configure :updated_at, :datetime do; label 'Atualizado em'; end   #   # Sections:
    # list do
    #   field :id
    #   field :titulo
    #   field :capa
    # end
    export do; end
    show do; end
    edit do; end
    create do; end
    update do; end
  end
  config.model Programa do
    # Found associations:
      configure :usuario, :belongs_to_association 
      configure :noticias, :has_many_association   #   # Found columns:
      configure :id, :integer 
      # configure :usuario_id, :integer         # Hidden 
      configure :nome, :string 
      configure :descricao, :text 
      configure :cor, :string 
      configure :posicao, :integer 
      configure :created_at, :datetime do
        label 'Criado em'
      end 
      configure :updated_at, :datetime do; label 'Atualizado em'; end   #   # Sections:
    list do; end
    export do; end
    show do; end
    edit do; end
    create do; end
    update do; end
  end
  config.model Usuario do
    label 'Usuário'
    label_plural 'Usuários'
    # Found associations:
    # Found columns:
      configure :id, :integer 
      configure :nome, :string 
      configure :email, :string 
      configure :senha, :string 
      configure :created_at, :datetime do
        label 'Criado em'
      end 
      configure :updated_at, :datetime do; label 'Atualizado em'; end   #   # Sections:
    list do; end
    export do; end
    show do; end
    edit do; end
    create do; end
    update do; end
  end
  config.model VideoNoticia do
    label 'Vídeo de notícia'
    label_plural 'Vídeos de notícia'
    # Found associations:
      configure :noticia#, :belongs_to_association   #   # Found columns:
      configure :id, :integer 
      # configure :noticia_id, :integer         # Hidden 
      configure :titulo, :string
      configure :url, :string
      configure :pagina_inicial, :boolean
      
      # configure :capa_file_name, :string         # Hidden 
      # configure :capa_content_type, :string         # Hidden 
      # configure :capa_file_size, :integer         # Hidden 
      # configure :capa_updated_at, :datetime         # Hidden 
      configure :capa, :paperclip 
      configure :created_at, :datetime do
        label 'Criado em'
      end 
      configure :updated_at, :datetime do; label 'Atualizado em'; end   #   # Sections:
    list do; end
    export do; end
    show do; end
    edit do; end
    create do; end
    update do; end
  end
  
  config.model VideoPrograma do
    label 'Vídeo de programa'
    label_plural 'Vídeos de programa'
    # Found associations:
      configure :programa#, :belongs_to_association   #   # Found columns:
      configure :id, :integer 
      # configure :noticia_id, :integer         # Hidden 
      configure :titulo, :string
      configure :url, :string 
      configure :pagina_inicial, :boolean
      
      # configure :capa_file_name, :string         # Hidden 
      # configure :capa_content_type, :string         # Hidden 
      # configure :capa_file_size, :integer         # Hidden 
      # configure :capa_updated_at, :datetime         # Hidden 
      configure :capa, :paperclip 
      configure :created_at, :datetime do
        label 'Criado em'
      end 
      configure :updated_at, :datetime do; label 'Atualizado em'; end   #   # Sections:
    list do; end
    export do; end
    show do; end
    edit do; end
    create do; end
    update do; end
  end
end
