class InicialController < ApplicationController
  def index
    @videos = VideoNoticia.group(:noticia_id).limit(6)
    @noticias = Noticia.limit(6).order('created_at DESC')
  end
end
