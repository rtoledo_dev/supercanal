class Noticia < ActiveRecord::Base
  belongs_to :programa
  
  has_many :video_noticias, class_name: 'VideoNoticia', table_name: 'video_noticias', :dependent => :delete_all, :autosave => true
  has_many :imagem_noticias, class_name: 'ImagemNoticia', table_name: 'imagem_noticias', :dependent => :delete_all, :autosave => true
  has_many :categoria_noticias, :dependent => :delete_all, :autosave => true
  has_many :categorias, :through => :categoria_noticias
  accepts_nested_attributes_for :categorias, :allow_destroy => true
  accepts_nested_attributes_for :video_noticias, :allow_destroy => true
  accepts_nested_attributes_for :imagem_noticias, :allow_destroy => true
  
  has_attached_file :capa, styles: { mini: "100x100#", normal: '338' },
                    default_style: :normal

  attr_accessible :bigode, :conteudo, :publicada_em, :tag, :titulo, 
  :categorias_attributes, :video_noticias_attributes, :imagem_noticias_attributes, 
  :programa_id, :capa

  validates_presence_of :bigode, :conteudo, :publicada_em, :titulo, :programa_id
  
  
  def to_param
    [id,titulo].join('-').parameterize
  end
end
